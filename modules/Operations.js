module.exports = function () {
    
      var opers = {
        
      //insert
  
          Insert: function (collection, data) {
              collection.insert(data, function (err, result) {                
                  //console.log(result)
              });
          },
  
          //select all - zwraca tablicę pasujących dokumentów
          SelectAll: function (collection, res) {
            function odpowiedz(items, res){
                res.writeHead(200, { 'Content-Type': 'text/html' });
                res.end(JSON.stringify(items))
            }
            collection.find({}).toArray(function (err, items) {
                odpowiedz(items, res)
            });
          },
  
          //select - zwraca tablicę pasujących dokumentów, z ograniczeniem
  
          SelectAndLimit: function (collection) {
              collection.find({login: "test"}).toArray(function (err, items) {
                  console.log(items)
              });
          },
  
          //delete - usunięcie poprzez id - uwaga na ObjectID
  
          DeleteById: function (ObjectID, collection, id) {
              collection.remove({ _id: ObjectID(id) }, function (err, data) {
              })
          },
  
          // update - aktualizacja poprzez id - uwaga na ObjectID
          // uwaga: bez $set usuwa poprzedni obiekt i wstawia nowy
          // z $set - dokunuje aktualizacji tylko wybranego pola
  
          UpdateById: function (ObjectID, collection, id, password){
              collection.updateOne(
                  { _id: ObjectID(id) },
                  { $set: { password: password } },
                  function (err, data) {                
                  })
          },
       
      }
  
      return opers;
  
  }