/*
    obsługa komunikację Ajax - serwer
*/
function Net() {
    this.sendData = function (message) {
        $.ajax({
            url: "http://localhost:3000/",
            data: message,
            type: "POST",
            success: function (data) {
                //czytamy odesłane z serwera dane
                var obj = JSON.parse(data)
                switch(obj.type){
                    case "connectServerRes":
                        console.log("response: ",obj)
                        if(obj.succes == "nie")
                            main.connectPrompt()
                        else
                            main.updateDatabases(obj.list)
                        break;
                    case "removeDatabaseRes":
                        $("#database_"+obj.remove).remove()
                        break;

                }
            },
            error: function (xhr, status, error) {
                console.log(xhr);
            },
        });
    }
}