function Main(){

    this.connectPrompt = function(){
        var connection = prompt("Wpisz adres IP:", "");
        if(connection != ""){
            net.sendData({connection: connection, action: "connectServer", database: "marchewka"})
            Settings.server = connection
        }else
            main.connectPrompt()
    }

    this.updateDatabases = function(arr){
        $("#chooseDatabase").empty()
        for(var i=0; i< arr.length; i++){
            option = document.createElement('option')
            option.value = arr[i].name
            option.id = "database_"+arr[i].name
            option.innerHTML = arr[i].name
            $("#chooseDatabase").append(option)
        }

        console.log(arr[0])
    }

    this.connectPrompt()
}