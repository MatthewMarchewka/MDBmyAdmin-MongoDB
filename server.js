var http = require("http");
var fs = require("fs");
var qs = require("querystring");
var mongoClient = require("mongodb").MongoClient;
var ObjectID = require('mongodb').ObjectID;
var Operations = require("./modules/Operations.js");

var database
var opers = new Operations();

var contentTypes = [
    {extension: "/", contentType: "text/html"},
    {extension: "css", contentType: "text/css"},
    {extension: "js", contentType: "text/plain"},
    {extension: "jpg", contentType: "image/jpeg"},
    {extension: "png", contentType: "image/png"},
]

function connectingDB(server, database, res, action){
    mongoClient.connect("mongodb://"+server+"/"+database, function (err, db) {
        if(err){
            res.writeHead(200, { 'Content-Type': 'text/html' });
            res.end(JSON.stringify({type: "connectServerRes", succes: "nie"}))
        }else{
            db.createCollection("kolekcja", function(err, coll){
                if(action == "removeDatabase"){
                    db.dropDatabase()
                    res.writeHead(200, { 'Content-Type': 'text/html' });
                    res.end(JSON.stringify({type: "removeDatabaseRes", remove: database}))
                }else{
                    db.admin().listDatabases(function(err,dbs,data){
                        if(err) console.log("nieudana lista")
                        else{
                            res.writeHead(200, { 'Content-Type': 'text/html' });
                            res.end(JSON.stringify({type: "connectServerRes", succes: "tak", list: dbs.databases}))
                        }
                    })
                }
            })
            coll = db.collection("kolekcja")
        }
    })
}

function servResponse(req,res){
    var allData = "";
    req.on("data", function (data) {
        allData += data;
    })
    req.on("end", function (data) {
        var response = ""
        var finishObj = qs.parse(allData)
        switch (finishObj.action) {
            //dodanie nowego usera
            case "connectServer":
            case "addDatabase":
            case "removeDatabase":
                connectingDB(finishObj.connection, finishObj.database, res, finishObj.action)
                break;
        }
    })
}

var server = http.createServer(function(request,response){
    switch (request.method) {
        case "GET":
        var file
        var contentType = null
        var extensionIndex = request.url.indexOf(".") 
        if(extensionIndex == -1){
            file = "static/index.html"
            contentType = "text/html"
        }else{
            for(var i=0; i<contentTypes.length; i++){
                if( request.url.slice(extensionIndex+1) == contentTypes[i].extension)
                    contentType = contentTypes[i].contentType
            }
            file = "static" + request.url
        }
        if(contentType){
            fs.readFile(file, function (error, data) {
                response.writeHead(200, { 'Content-Type': contentType });
                response.write(data);
                response.end();
            })
        }
        break;
        case "POST":
            servResponse(request,response)
            break;
    
    } 
})

server.listen(3000, function(){
   console.log("serwer startuje na porcie 3000")
});

// mongoClient.connect("mongodb://192.168.1.208/marchewka", function (err, db) {
//     if (err) console.log(err)
//     else console.log("mongo podłączone")
//     //tu można operować na utworzonej bazie danych db lub podstawić jej obiekt 
//     // pod zmienną widoczną na zewnątrz
//     database = db

//     db.createCollection("users", function (err, coll) {
        
//     })
    
//     // opers.SelectAndLimit (coll,function (data) {            
//     //     console.log(data)
//     // })    
// })

